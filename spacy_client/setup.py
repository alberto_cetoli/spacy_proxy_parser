from setuptools import setup

setup(
    name='cscout_displacy_client',
    version='0.1.1',
    description='Client for the cscout_displacy_service',
    include_package_data=True,
)