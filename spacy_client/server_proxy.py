import json
import requests
import logging


class BaseServerProxy(object):
    """
    Base class for connecting to the server. It performs POST requests onto the server url.
    """

    _logger = logging.getLogger(__name__)
    _headers = {'content-type': 'application/json'}

    def get_json(self, data):
        """
        :param data: The data to send to the server
        :return: The json with the response from the server
        """
        raise NotImplementedError('BaseServerProxy needs to be implemented in the child class')

    def _post_and_get_json(self, url, data):
        """
        Performs the actual POST request.

        :param url: The url of the server
        :param data: The data to send
        :return: The response from the server
        """
        try:
            response = requests.post(url, data=json.dumps(data), headers=self._headers)
            return response.json()
        except Exception as e:
            self._logger.error(str(e))
            raise e


class SpacyDependencyServerProxy(BaseServerProxy):
    """
    Connects to the part of the server that deals with dependency parsing
    """

    def __init__(self, server_url):
        self.dependency_url = server_url + '/dep'

    def get_json(self, data):
        return BaseServerProxy._post_and_get_json(self, self.dependency_url, data)


class SpacyEntityServerProxy(BaseServerProxy):
    """
    Connects to the part of the server that deals with NER
    """

    def __init__(self, server_url):
        self.entity_url = server_url + '/ent'

    def get_json(self, data):
        return BaseServerProxy._post_and_get_json(self, self.entity_url, data)
