from spacy_client.server_proxy import SpacyDependencyServerProxy, SpacyEntityServerProxy

_default_model_name = 'en'


class Token(object):
    def __init__(self, index, text, tag, label, lemma, entity, head, idx, vector, children):
        """
        This is merely a structure that contains the information of each token.

        :param index: The index of this token
        :param text: The text of this token
        :param tag: The part-of-speech (POS) tag of this token
        :param label: The dependency label of the edge that points to this token ('nsubj', 'prep', ...)
        :param children: 'The indices of the children tokens to this one'
        """
        import numpy as np

        self.i = index
        self.orth_ = text
        self.tag_ = tag
        self.dep_ = label
        self.lemma_ = lemma
        self.head = head
        self.ent_type_ = entity
        self.idx = int(idx)
        self.vector = np.array(vector)
        self.children = children


class Entity:
    def __init__(self, text, label, start_char, end_char):
        """
        This is merely a structure that contains the information of each entity.

        :param text: The text of this entity
        :param label: The entity type
        :param start_char: the starting position of this entity in the document
        :param end_char: the ending position of this entity in the document
        """
        self.text = text
        self.label_ = label
        self.start_char = start_char
        self.end_char = end_char


class Tokens(object):
    def __init__(self):
        self._tokens = []
        self.ents = []

    def append(self, item):
        if not isinstance(item, Token):
            raise AttributeError('The item ' + item + ' is not an instance of Token()')
        self._tokens.append(item)
        self.ents.append(Entity(text=item.orth_,
                                label=item.ent_type_,
                                start_char=item.idx,
                                end_char=item.idx + len(item.orth_) - 1))

    def __getitem__(self, index):
        return self._tokens[index]

    def __len__(self):
        return len(self._tokens)


class BaseClient(object):
    """
    Base class for the client proxy
    """
    _model_name = _default_model_name

    def _create_input_for_proxies(self, message_text):
        """
        Translates the message text into a dict that can be digested by the server
        :param message_text: The message to send to the server
        :return: The dict to send to the server
        """
        return {'text': message_text,
                'model': self._model_name,
                'collapse_punctuation': False,
                'collapse_phrases': False,
                }


class SpacyParserProxy(BaseClient):
    def __init__(self, url, model_name=_default_model_name):
        """
        This class connects to the server and compute a dependency tree

        :param url: The url to connect to
        :param model_name: The Spacy model to use
        """

        self._server_proxy = SpacyDependencyServerProxy(url)
        self._model_name = model_name

    def __call__(self, sentence):
        """
        This method actuall to the parsing of the sentence

        :param sentence: The sentence to be parsed
        :return: a list of Token.
        """
        data = BaseClient._create_input_for_proxies(self, sentence)
        dependency_json = self._server_proxy.get_json(data)
        return self.__create_tokens(dependency_json)

    # Private

    def __create_tokens(self, dependency_json):
        words_dict_list = dependency_json['words']
        tokens = self.__create_tokens_with_text_and_tag(words_dict_list)
        tokens = self.__add_children_nodes_to_tokens(tokens, self.__create_edges_dict(dependency_json['arcs']))
        return tokens

    def __create_tokens_with_text_and_tag(self, words_dict_list):
        tokens = Tokens()
        for index, word in enumerate(words_dict_list):
            tokens.append(Token(index=index,
                                text=word['text'],
                                tag=word['tag'],
                                lemma=word['lemma'],
                                entity=word['entity'],
                                head=word['head'],
                                idx=word['idx'],
                                vector=word['vector'],
                                label='',
                                children=[]
                                ))
        return tokens

    def __create_edges_dict(self, arcs_dict_list):
        labels_dict = {}
        children_dict = {}
        for item in arcs_dict_list:
            start = 'start'
            end = 'end'
            if item['dir'] == 'left':
                start = 'end'
                end = 'start'
            labels_dict[item[end]] = item['label']
            try:
                children_dict[item[start]].append(item[end])
            except:
                children_dict[item[start]] = [item[end]]
        return {'children': children_dict, 'labels': labels_dict}

    def __add_children_nodes_to_tokens(self, tokens, edges_and_labels):
        children_dict = edges_and_labels['children']
        labels_dict = edges_and_labels['labels']
        for index, token in enumerate(tokens):
            if index in labels_dict:
                token.dep_ = labels_dict[index]
            if index in children_dict:
                token.children = children_dict[index]
        return tokens


class SpacyNERProxy(BaseClient):
    def __init__(self, url, model_name='en'):
        """
        This class connects to the server and compute the entities in a sentence

        :param url: The url to connect to
        :param model_name: The Spacy model to use
        """
        self._server_proxy = SpacyEntityServerProxy(url)
        self._model_name = model_name

    def __call__(self, sentence):
        """
        This method actuall to the parsing of the sentence

        :param sentence: The sentence to be parsed
        :return: a list of dicts, with one dict for each entity:
                  {'start': the position (in characters) of the start of the entity,
                   'end': the position (in characters) of the end of the entity,
                   'text': the text of the entity,
                   'type': the type of the entity
                   }.
        """
        data = BaseClient._create_input_for_proxies(self, sentence)
        return self._server_proxy.get_json(data)
