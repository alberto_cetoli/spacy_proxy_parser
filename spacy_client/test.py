from spacy_client.client_proxies import SpacyParserProxy, SpacyNERProxy

#_url = "http://35.189.89.100"
#_url = "http://35.189.123.249"
_url = "http://localhost:8000"
_message_text = "Jane Doe ate the pizza with anchovies. She liked the pizza."

if __name__ == '__main__':

    print('\nCalling the parser remotely')
    parser = SpacyParserProxy(_url, model_name='en_core_web_md')
    tokens = parser(_message_text)
    for token in tokens:
        print(token.idx, token.orth_, token.tag_, token.lemma_, token.ent_type_, token.children, token.dep_, token.head, token.vector)

    print('\nPrinting the entities')
    for e in tokens.ents:
        print(e.text, e.label_, e.start_char, e.end_char)

    print('\nCalling the NER remotely')
    ner = SpacyNERProxy(_url, model_name='en_core_web_md')
    ner_items = ner(_message_text)
    [print(item) for item in ner_items]
