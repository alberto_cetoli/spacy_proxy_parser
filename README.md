Spacy Proxy Parser
------------------

Test usage:
```python
from spacy_client.client_proxies import SpacyParserProxy, SpacyNERProxy

_url = "http://35.189.123.249"
_message_text = "Jane Doe ate the pizza with anchovies. She liked the pizza."

if __name__ == '__main__':

    print('\nCalling the parser remotely')
    parser = SpacyParserProxy(_url, model_name='en_core_web_md')
    tokens = parser(_message_text)
    for token in tokens:
        print(token.idx, token.orth_, token.tag_, token.children, token.dep_)

    print('\nCalling the NER remotely')
    ner = SpacyNERProxy(_url, model_name='en_core_web_md')
    ner_items = ner(_message_text)
    [print(item) for item in ner_items]
```

with output
```bash

Calling the parser remotely
0 Jane NNP [] compound
1 Doe NNP [0] nsubj
2 ate VBD [1, 4, 5] 
3 the DT [] det
4 pizza NN [3] dobj
5 with IN [6] prep
6 anchovies NNS [] pobj

Calling the NER remotely
{'end': 8, 'type': 'PERSON', 'text': 'Jane Doe', 'start': 0}
```


TODO
====

* Should it return the sentences as a list? Spacy does that but not through the REST API
* Tokens do not have lemmas! => Modify the Server code?
* Should you add the entity types to the Server code?